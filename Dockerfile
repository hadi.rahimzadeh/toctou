FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /source

# install deps and cache to speed up
COPY source.sln .
COPY src/program.csproj src/program.csproj
COPY test/test.csproj test/test.csproj
RUN dotnet restore

# test
COPY src src
COPY test test
ARG TESTSPEC="usability"
RUN dotnet test --filter TestCategory=$TESTSPEC

# publish app and libraries
RUN dotnet publish -c debug -o /app --no-restore

# final stage/image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "program.dll"]
